## === Post Process results ===
## This script corrects results files for 2 Bugs:
## - Wrong expression when user leave or refresh the page, due to the random selection of video list before loading localStorage data.
## - 'Name' field dont correspond of the real id of the csv score file, due to a tricky text format in the csv file


import csv
import json
import pathlib

results_path = "results"
results_output_path = "results"
csv_clips_filepath = "AD_sorted_for_EmoTag.csv"

csv_reader = csv.reader(open(csv_clips_filepath), delimiter="|")
csv_data = list(csv_reader)
clips = [clip[0] for clip in csv_data]

emotions = [
    "COLERE",
    "DESOLE",
    "DETERMINE",
    "ENTHOUSIASTE",
    "ESPIEGLE",
    "ETONNE",
    "EVIDENCE",
    "INCREDULE",
    "NEUTRE",
    "PENSIF",
    "RECONFORTANT",
    "SUPPLIANT",
]

def get_mapping(csv_clips_filepath):

    csv_reader_source = csv.reader(open(csv_clips_filepath), delimiter="|")
    csv_data_source = list(csv_reader_source)
    clips_source = [clip[0] for clip in csv_data_source]

    csv_reader_target = csv.reader(open(csv_clips_filepath), delimiter="|", quotechar = '£')
    csv_data_target = list(csv_reader_target)
    clips_target = [clip[0] for clip in csv_data_target]

    mapping_ids = {}

    i_source = 0
    for i_target, data_target in enumerate(csv_data_target):
        data_source = csv_data_source[i_source]
        if data_target == data_source:
            mapping_ids[i_source] = i_target
            i_source +=1
        else:
            if data_target[0:2]==csv_data_source[i_source][0:2]:
                mapping_ids[i_source] = i_target
                i_source +=1
    return mapping_ids

mapping_ids = get_mapping(csv_clips_filepath)

result_files = list(pathlib.Path(results_path).rglob("*.json"))



for result_file in result_files:
    result = json.load(open(result_file))
    isBadExpression = False

    with open(results_output_path + "/" +  result_file.name, "w") as f:

        for review in result["reviews"]:
            ## Replace bad expression by true expression 
            video_index = int(review['name'])
            expression = review['expression']
            clip_stem = clips[video_index]
            true_expression = [emotion for emotion in emotions if emotion in clip_stem] or ["NEUTRE"]
            true_expression = true_expression[0]
            if expression != true_expression:
                review['expression'] = true_expression
                isBadExpression = True

            ## Add field with corrected id
            review['csv_id'] = mapping_ids[video_index]
            review['video_name'] = clip_stem
        

        # manual correction for the score of training review
        result["training"][0]['score']='0.639'

        json.dump(result, f, indent=4)

        if isBadExpression:
            print(f"Apply correction for expression in : {result_file}")
