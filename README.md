# Emotags-results

Les résultats sont sauvergardés dans le volume persistant du conteneur docker de l'application :
`~/www/emotags/results`
Pour les récuperer : 
`scp -r ubuntu@129.88.204.192:~/www/emotags/results .`

Puis executer le script de post traitement : 
`python post_process_results.py`
Le script ajoute le champ `csv_id` qui fait le mapping entre l'id utilisé par le champ `name` et l'id du numéro de ligne du fichier `AD_sorted_for_EmoTag.csv`
Il corrige également un bug sur une mauvaise expression écrite lors d'un cas particulier de rafraichement du site web.

Pour mettre à jour le notebook :  
installer jupyter :`pip install jupyter pandas seaborn`  
executer le notebook dans un navigateur, ou utiliser la commande : `jupyter nbconvert --execute --clear-output analyse_prolific.ipynb`  

# Version quasi automatisée :

La première fois, cloner ce repo:\
`git clone https://`YOUR_LOGIN`@gricad-gitlab.univ-grenoble-alpes.fr/web/emotags-results.git`\
qui créera un répertoire `emotags_results` contenant les données et la dernière version du script `post_process_results.py`

Les fois suivantes, une fois connecté au VPN UGA, par exemple avec `/opt/cisco/anyconnect/bin/vpnui`\
`cd emotags-results/`\
`scp -r ubuntu@129.88.204.192:/home/ubuntu/www/emotags/results .`\
`python3.8 post_process_results.py`\
`git add results`\
`git commit -m "update results"`\
`jupyter nbconvert --execute --clear-output analyse_prolific.ipynb`\
`git add analyse_prolific.ipynb`\
`git commit -m "update Notebook"`\
`git push`
